﻿using System.IO;
using System.Threading.Tasks;
using FileTras.DataBase;
using FileTras.DataBase.EntityModels;
using FileTrash.Core;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FileTrash.API.Controllers
{
    [Route("taskController")]
    [ApiController]
    public class TaskController : Controller
    {
        AppContext _context;
        IWebHostEnvironment _appEnvironment;
 
        public TaskController(IWebHostEnvironment appEnvironment, AppContext appContext)
        {
            _context = appContext;
            _appEnvironment = appEnvironment;
        }
        [HttpPost]
        public async Task<bool> AddFile(IFormFile uploadedFile)
        {
            if (uploadedFile != null)
            {
                // путь к папке Files
                string path = "/Files/" + uploadedFile.FileName;
                // сохраняем файл в папку Files в каталоге wwwroot
                using (var fileStream = new FileStream(_appEnvironment.WebRootPath + path, FileMode.Create))
                {
                    await uploadedFile.CopyToAsync(fileStream);
                }
                FileModel file = new FileModel { Name = uploadedFile.FileName, Path = path };
                _context.FileModels.Add(file);
                _context.SaveChanges();
                return true;
            }

            return false;
        }
    }
}