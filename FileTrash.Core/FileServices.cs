﻿using System;
using System.IO;
using FileTras.DataBase.EntityModels;

namespace FileTrash.Core
{
    public class FileService
    {
        private FileModel _file = new FileModel();
        private string _newPath;
        private string _path;

        public FileService(string newPath, string path)
        {
            _newPath = newPath;
            _path = path;
        }

        // public string MoveFile()
        // {
        //    
        //     _file.NewPath = _newPath;
        //     _file.Path = _path;
        //     
        //     FileInfo file = new FileInfo(_file.Path);
        //     if (file.Exists)
        //     {
        //         file.MoveTo(_file.NewPath);
        //     }
        //     return _file.NewPath;
        // }

        public string FileInfo()
        {
        
            _file.Path = _path;
            FileInfo fileInfo = new FileInfo(_file.Path);
            if (fileInfo.Exists)
            {
                return $"Имя файла: {{fileInfo.Name}}, Время создания: {fileInfo.CreationTime}, Размер: {fileInfo.Length}";
            }
            return $"File does not EXIST!!!";
        }

        public void FileDelete()
        {
            
            _file.Path = _path;
            FileInfo fileInfo = new FileInfo(_file.Path);
            if (fileInfo.Exists)
            {
                fileInfo.Delete();
            }
        }

        // public void FileCopy()
        // {
        //
        //     _file.Path = _path;
        //     _file.NewPath = _newPath;
        //     FileInfo fileInfo = new FileInfo(_file.Path);
        //     if (fileInfo.Exists)
        //     {
        //         fileInfo.CopyTo(_newPath,true);
        //     }
        // }
    }
}